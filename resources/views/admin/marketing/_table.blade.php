<?php use App\Traits\SortableTrait; ?>
@if(count($data))
    <table class="table table-striped">
        <thead>
        <tr>
            <th>{!! SortableTrait::linkZoSortingAction('id', 'ID') !!}</th>
            <th>{!! SortableTrait::linkZoSortingAction('name', 'Ime i Prezime') !!}</th>
            <th>{!! SortableTrait::linkZoSortingAction('email', 'Email') !!}</th>
            <th>{!! SortableTrait::linkZoSortingAction('phone', 'Telefon') !!}</th>
            <th>{!! SortableTrait::linkZoSortingAction('client', 'Klijent') !!}</th>
            <th>{!! SortableTrait::linkZoSortingAction('age', 'Starosna dob') !!}</th>
            <th>{!! SortableTrait::linkZoSortingAction('ip_address', 'IP adresa') !!}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
            <tr>
                <td>{{$row->id}}</td>
                <td>{{$row->name}}</td>
                <td>{{$row->email}}</td>
                <td>{{$row->phone}}</td>
                <td>{{$row->client ? 'DA' : 'NE'}}</td>
                <td>{{$row->age_title}}</td>
                <td>{{$row->ip_address}}</td>
            </tr>
        @endforeach
        </tbody>
        @if($data->count() > 15)
            <tfoot>
            <tr>
                <td colspan="3" class="text-right" style="margin: 0;">
                    {{$data->appends($input)->links()}}
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    @include('admin.widgets.alert', ['class'=>'alert-info', 'message'=>'Nema podataka.'])
@endif