require('jquery-validation');

// override jquery validate plugin defaults
$.validator.setDefaults({
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

$.extend($.validator.messages, {
    required: "Polje je obavezno.",
    email: "Polje mora biti ispravna email adresa."
});

$('#login-form').validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true
        }
    }
});

$('#jmbg-filters').validate({
    rules: {
        date_from: {
            date: true
        },
        date_to: {
            date: true
        }
    }
});