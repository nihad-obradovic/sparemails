<form name="jmbg-filters" id="jmbg-filters" class="form-inline" method="get">
    <div class="form-group">
        <label><strong>OD:</strong>&nbsp;</label>
        <input type="text" name="date_from" class="form-control input-sm datepicker" value="{{$date_from}}">
    </div>
    &emsp;
    <div class="form-group">
        <label><strong>DO:</strong>&nbsp;</label>
        <input type="text" name="date_to" class="form-control input-sm datepicker" value="{{$date_to}}">
    </div>
    &emsp;
    <div class="form-group">
        <input type="submit" class="btn btn-primary btn-sm" value="Filtriraj">
        <a href="{{route('admin.jmbgs')}}" class="btn btn-danger btn-sm">Poništi</a>
    </div>
</form>