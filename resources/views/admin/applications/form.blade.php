@extends('admin.layouts.dashboard')

@section('page_heading', 'Prijava Test')

@section('section')
    <div class="col-sm-6" id="applications-form-container">
        <form name="applications-form" id="applications-form">
            @component('admin.widgets.panel')
                @slot('panelBody')
                    <div class="form-group">
                        <label>JMBG: </label>
                        <input type="text" name="jmbg" class="form-control">
                        <p id="jmbg-errors" class="text-danger"></p>
                    </div>
                    <div class="form-group">
                        <label>Email: </label>
                        <input type="email" name="email" class="form-control">
                        <p id="email-errors" class="text-danger"></p>
                    </div>                    
                    <div class="form-group">
                        <label>
                           <input type="checkbox" name="confirmed"> 
                           Unosom gore navedenih podataka, saglasan sam da Sparkasse Bank d.d. BiH čuva, obrađuje i koristi moje lične podatke iz ovog ispunjenog obrasca: jedinstveni matični broj (JMBG) i e-mail adresu, u svrhu ažuriranja podataka koje banka vodi o svojim klijentima, promotivnih i prodajnih aktivnosti, obavještavanja o novostima i ponudama Banke, te direktnog marketinga, analize i istraživanja. Ova saglasnost se ne može koristiti u druge svrhe. Ovu saglasnost dajem na neograničen vremenski period, do vlastitog opoziva iste.                     
                        </label>
                        <p id="confirmed-errors" class="text-danger"></p>
                    </div>
                @endslot
                @slot('panelFooter')
                    <input type="submit" class="btn btn-primary btn-sm" value="Dodaj" disabled>
                @endslot
            @endcomponent
        </form>
    </div>
    <div class="col-sm-6" id="success-message-container" style="display: none">
        <div class="alert alert-success" role="alert">
            Hvala Vam za učešće u nagradnoj igri.
        </div>
    </div>

    <script src="{{ asset("js/form.js") }}"></script>
@endsection
