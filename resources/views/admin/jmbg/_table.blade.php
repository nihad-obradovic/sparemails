<?php use App\Traits\SortableTrait; ?>
@if(count($data))
    <table class="table table-striped">
        <thead>
        <tr>
            <th>{!! SortableTrait::linkZoSortingAction('id', 'ID') !!}</th>
            <th>{!! SortableTrait::linkZoSortingAction('jmbg', 'JMBG') !!}</th>
            <th>{!! SortableTrait::linkZoSortingAction('applied', 'Potvrđen') !!}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
            <tr>
                <td>{{$row->id}}</td>
                <td>{{$row->jmbg}}</td>
                <td>
                    @if($row->applied)
                        @include('admin.widgets.badge', ['value' => 'Potvrđen', 'class'=>'success'])
                    @else
                        @include('admin.widgets.badge', ['value' => 'Nije potvrđen', 'class'=> 'failure'])
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
        @if($data->count() > 15)
            <tfoot>
            <tr>
                <td colspan="3" class="text-right" style="margin: 0;">
                    {{$data->appends($input)->links()}}
                </td>
            </tr>
            </tfoot>
        @endif
    </table>
@else
    @include('admin.widgets.alert', ['class'=>'alert-info', 'message'=>'Nema podataka.'])
@endif