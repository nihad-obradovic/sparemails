<?php

namespace App\Http\Controllers;

use App\Application;
use App\Jmbg;
use App\Marketing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    public function marketing(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'name' => 'required|min:5',
            'email' => 'required|email|unique:marketing',
            'phone' => 'required|digits:9',
            'age' => 'required|numeric',
            'client' => 'required|numeric',
            'disclaimer' => 'required|accepted',
        ]);

        if($validator->fails())
        {
            return response()->json([
                'created'=>false,
                'errors'=>$validator->errors()
            ]);
        }

        $created = Marketing::createFromRequest($request);

        return response()->json([
            'created' => $created
        ]);

    }

    public function check(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'jmbg' => 'required|digits:13',            
            'email' => 'required|email'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'valid' => false
            ]);
        }

        $valid = $this->checkIfJmbgApplied($request->input('jmbg'));
        $exists = $this->checkIfEmailApplied($request->input('email'));

        return response()->json([
            'valid' => $valid,
            'exists' => $exists
        ]);
    }

    public function apply(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'jmbg' => 'required|digits:13',
            'email' => 'required|email',
            'confirmed' => 'required|accepted'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'valid' => false
            ]);
        }

        $valid = $this->checkIfJmbgApplied($request->input('jmbg'));
        $exists = $this->checkIfEmailApplied($request->input('email'));
        if($valid && !$exists)
        {
            $created = Application::createFromRequest($request);
        }

        return response()->json([
            'valid' => $valid,
            'exists' => $exists,
            'created' => isset($created) ? $created : false
        ]);

    }

    private function checkIfJmbgApplied($jmbg)
    {
        $count = Jmbg::where('jmbg', $jmbg)
            ->where('applied', 0)
            ->count();

        return $count > 0;
    }

    private function checkIfEmailApplied($email)
    {
        $count = Application::where('email', $email)
            ->count();

        return $count > 0;
    }
}
