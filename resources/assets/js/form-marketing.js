
// try {
//    window.$ = window.jQuery = require('jquery');
// } catch (e) {}

$("[name=disclaimer]").change(function(){

    if($("#disclaimer1").is(":checked")) {
        $("#form-submit").show();
    } else {
        $("#form-submit").hide();
    }
    
    checkDisclaimer($("#disclaimer1").is(":checked") ? 1 : 0);
});

$('#marketing-form').submit(function(event){
    event.preventDefault();

    var form = $('#marketing-form').serializeArray();

    var data = {};
    form.forEach(function(item) {
        data[item.name] = item.value;
    });

    var error = false;

    error |= checkName(data.name);
    error |= checkEmail(data.email);
    error |= checkPhone(data.phone);
    error |= checkAge(data.age);
    error |= checkClient(data.client);
    error |= checkDisclaimer(data.disclaimer);

    if(error) return;

    $("#form-submit").attr('disabled','disabled');
    $.post('https://proxy.diwdev.info/marketing.php', data)
        .done(function(response){
            if(!response.created) {
                for (var key in response.errors) {
                    $('#'+key+'-errors').html(response.errors[key].join('<br>'));
                }
            } else {
                $('#marketing-form-container').hide();
                $('#success-message-container').show();
            }
            $("#form-submit").removeAttr('disabled');
        })
        .fail(function(xhr, status, error) {
            console.log("Error");
            $("#form-submit").removeAttr('disabled');
        });
});


function checkDisclaimer(disclaimer) {
    var error = false;

    if(parseInt(disclaimer) !== 1 ) {
        $('#disclaimer-errors').html('Morate se složiti sa disclaimer-om.');
        error = true;
    } else {
        $('#disclaimer-errors').html('');
    }

    return error;
}

function checkClient(client) {
    var error = false;

    if(! client) {
        $('#client-errors').html('Odaberite da li ste klijent banke.');
        error = true;
    } else {
        $('#client-errors').html('');
    }

    return error;
}

function checkAge(age) {
    var error = false;

    if(! age) {
        $('#age-errors').html('Odaberite starosnu dob.');
        error = true;
    } else {
        $('#age-errors').html('');
    }

    return error;
}

function checkPhone(phone) {
    var error = false;

    if(phone.length < 9) {
        $('#phone-errors').html('Unesite ispravan broj telefona.');
        error = true;
    } else {
        $('#phone-errors').html('');
    }

    return error;
}

function checkName(name) {
    var error = false;

    if(name.length < 5) {
        $('#name-errors').html('Unesite vaše ime i prezime.');
        error = true;
    } else {
        $('#name-errors').html('');
    }

    return error;
}

function checkEmail(email) {
    var error = false;

    if(! validateEmail(email)) {
        $('#email-errors').html('Unesite ispravnu email adresu.');
        error = true;
    } else {
        $('#email-errors').html('');
    }

    return error;
}
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}