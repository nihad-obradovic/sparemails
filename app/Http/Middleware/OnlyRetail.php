<?php

namespace App\Http\Middleware;

use Closure;

class OnlyRetail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->marketing) {
            return redirect('/admin/marketing/list');
        }

        return $next($request);
    }
}
