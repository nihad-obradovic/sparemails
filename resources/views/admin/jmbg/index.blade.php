@extends('admin.layouts.dashboard')

@section('page_heading','Matični brojevi')

@section('section')
    <div class="col-sm-12 col-md-10 col-lg-8">
        @component('admin.widgets.panel')
            @slot('panelBody')
                @include('admin.jmbg._filter', [
                    'date_from' => isset($input['date_from']) ? $input['date_from'] : '',
                    'date_to' => isset($input['date_to']) ? $input['date_to'] : ''
                ])
            @endslot
            @slot('panelTable')
                @include('admin.jmbg._table', ['data'=>isset($jmbgs) ? $jmbgs : []])
                <div class="text-center">
                    {{$jmbgs->links()}}
                </div>
            @endslot
        @endcomponent
    </div><!-- /.col-sm-6 -->
    <div class="col-sm-12 col-md-2 col-lg-4">
        @include('admin.jmbg._import')
    </div><!-- /.col-sm-4 -->
@endsection
