<?php namespace App\Exceptions;

/**
 * Class RepositoryException
 * @package Bosnadev\Repositories\Exceptions
 */
class RepositoryException extends \Exception
{

}