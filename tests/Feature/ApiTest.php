<?php

namespace Tests\Feature;

use App\Jmbg;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiTest extends TestCase
{
    use WithoutMiddleware, DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCheck()
    {
        $response = $this->post('/api/check');
        $response->assertExactJson([
            "valid" => false
        ]);

        // check if JMBG is not valid if does not exist in the system
        $response = $this->post('/api/check', [
            'jmbg' => '1234567890123',
            'email' => 'email@email.com',
            'confirmed' => '1'
        ]);
        $response->assertExactJson([
            'exists' => false,
            "valid" => false
        ]);

        // create jmbg
        $jmbg = Jmbg::create([
            'jmbg' => '1234567890123',
            'import_data' => "Test"
        ]);
        $create = [
            'jmbg' => $jmbg->jmbg,
            'email' => 'email@email.com',
            'confirmed' => '1'
        ];

        // check if JMBG is valid if exists in the system
        $response = $this->post('/api/check', $create);
        $response->assertExactJson([
            "exists" => false,
            "valid" => true
        ]);

        // check if application is created if applied
        $response = $this->post('/api/apply', $create);
        $response->assertExactJson([
            "exists" => false,
            "valid" => true,
            "created" => true
        ]);

        // check if jmbg is valid if already registered
        $response = $this->post('/api/check', $create);
        $response->assertExactJson([
            "exists" => true,
            "valid" => false
        ]);
    }
}
