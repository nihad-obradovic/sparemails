
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));
//
// const app = new Vue({
//     el: '#app'
// });

// require('node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min');

// form validation
require('./form-validation');

require('jquery-ui-bundle');

$( function() {
    if($('.datepicker').length>0) {
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd",
            regional: { // Default regional settings
                closeText: "Done", // Display text for close link
                prevText: "Prev", // Display text for previous month link
                nextText: "Next", // Display text for next month link
                currentText: "Today", // Display text for current month link
                monthNames: [ "January","February","March","April","May","June",
                    "July","August","September","October","November","December" ], // Names of months for drop-down and formatting
                monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ], // For formatting
                dayNames: [ "Nedelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota" ], // For formatting
                dayNamesShort: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ], // For formatting
                dayNamesMin: [ "Su","Mo","Tu","We","Th","Fr","Sa" ], // Column headings for days starting at Sunday
                weekHeader: "Wk", // Column header for week of the year
                dateFormat: "mm/dd/yy", // See format options on parseDate
                firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
                isRTL: false, // True if right-to-left language, false if left-to-right
                showMonthAfterYear: false, // True if the year select precedes month, false for month then year
                yearSuffix: "" // Additional text to append to the year in the month headers
            }
        });

    }
});