<?php

namespace App\Http\Middleware;

use Closure;

class OnlyMarketing
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->user()->marketing) {
            return redirect('admin.home');
        }

        return $next($request);
    }
}
