<?php

namespace App\Http\Controllers\Admin;

use Excel;
use App\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Psy\Formatter\Formatter;

class ApplicationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $input = $request->input();
        $applications = Application::filterFromRequest($input)->paginate(15);

        return view('admin.applications.list', compact('applications', 'input'));
    }

    public function export(Request $request)
    {
	    $input = $request->input();
        $applications = Application::filterFromRequest($input)->get()->toArray();

        if(count($applications) == 0)
        {
            return redirect()->back();
        }

        $data = [];
        foreach($applications as $item)
        {
            $item['browser_info'] = json_encode($item['browser_info']);
            $data[] = $item;
        }

        $excel = Excel::create('export_'.date('Ymd_Hi'), function($excel) use ($data)
        {
            $excel->sheet('Sheet 1', function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        });

        // work on the export
        return $excel->download('csv');
    }

    public function form()
    {
        return view('admin.applications.form');
    }
}
