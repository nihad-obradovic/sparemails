<?php namespace App\Repositories;

use App\Importers\JmbgImporter;
use App\Repository;
use App\Jmbg;

class JMBGRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return Jmbg::class;
    }

    public function filter($request)
    {
        $data = $this->model->sortable();

        if(isset($request))
        {
            if($request['date_from'])
            {
                $data = $data->whereDate('created_at', '>=', $request['date_from']);
            }
            if($request['date_to'])
            {
                $data = $data->whereDate('created_at', '<=', $request['date_to']);
            }
        }

        return $data;
    }

    public function importFiles($files)
    {
        $importer = new JmbgImporter();

        foreach ($files as $file)
        {
           $importer->importFile($file);
        }

        return [
           'imported' => $importer->imported,
           'errors' => $importer->errors,
        ];
    }
}