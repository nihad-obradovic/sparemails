<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jmbg;
use App\Repositories\JMBGRepository;
use Illuminate\Http\Request;

class JmbgController extends Controller
{
    private $jmbg;

    /**
     * Create a new controller instance.
     *
     * @param JMBGRepository $jmbg
     */
    public function __construct(JMBGRepository $jmbg)
    {
        $this->jmbg = $jmbg;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $input = $request->input();
        $jmbgs = $this->jmbg->filter($request)->paginate(15);

        return view('admin.jmbg.index', compact('jmbgs', 'input'));
    }

    public function import(Request $request)
    {
        $files = $request->file('import_files');

        $result = $this->jmbg->importFiles($files);

        return view('admin.jmbg.import_details', $result);
    }
}
