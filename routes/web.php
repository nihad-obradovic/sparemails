<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return "Welcome";
});


Route::group(['middleware' => 'fw-only-whitelisted'], function () 
{
    // Authentication Routes...
    Route::get('admin/login', [
        'as' => 'admin.login',
        'uses' => 'Admin\LoginController@showLoginForm'
    ]);
    Route::post('admin/login', [
        'as' => 'admin.login.post',
        'uses' => 'Admin\LoginController@login'
    ]);
    Route::post('admin/logout', [
        'as' => 'admin.logout',
        'uses' => 'Admin\LoginController@logout'
    ]);

    Route::group(['middleware' => 'retail-only'], function ()
    {
        // Dashboard
        Route::get('/admin/dashboard', 'HomeController@index')->name('admin.dashboard');

        // JMBG routes
        Route::get('/admin/jmbgs', 'Admin\JmbgController@index')->name('admin.jmbgs');
        Route::post('/admin/jmbgs/import', 'Admin\JmbgController@import')->name('admin.jmbgs.import');

        // Applications routes
        Route::get('/admin/applications/list', 'Admin\ApplicationController@index')->name('admin.applications.list');
        Route::post('/admin/applications/export', 'Admin\ApplicationController@export')->name('admin.applications.export');
        Route::get('/admin/applications/form', 'Admin\ApplicationController@form')->name('admin.applications.form');
    });
    Route::group(['middleware' => 'marketing-only'], function ()
    {
        // Applications routes
        Route::get('/admin/marketing/list', 'Admin\MarketingController@index')->name('admin.marketing.list');
        Route::post('/admin/marketing/export', 'Admin\MarketingController@export')->name('admin.marketing.export');
        Route::get('/admin/marketing/form', 'Admin\MarketingController@form')->name('admin.marketing.form');
    });
});

// API routes
Route::post('api/check', 'ApiController@check')->middleware('cors');
Route::post('api/apply', 'ApiController@apply')->middleware('cors');
Route::post('api/marketing', 'ApiController@marketing')->middleware('cors');
