var CHECK_URL = "https://proxy.diwdev.info/check.php";
var APPLY_URL = "https://proxy.diwdev.info/apply.php";

var submitBtn = document.querySelector('#applications-form [type=submit]');
submitBtn.removeAttribute("disabled");
submitBtn.style.display = 'none';

var confirmBox = document.querySelector('#applications-form [name=confirmed]');

confirmBox.onchange = function(el){
    if(el.target.checked) {
        submitBtn.style.display = 'initial';
    } else {
        submitBtn.style.display = 'none';
    }
};

var applicationForm = document.getElementById('applications-form');

var jmbgErrors = document.getElementById('jmbg-errors');
var emailErrors = document.getElementById('email-errors');
var confirmedErrors = document.getElementById('confirmed-errors');

function submitting() {
    submitBtn.disabled = true;
}

function doneSubmitting() {
    submitBtn.disabled = false;
}

applicationForm.onsubmit = function(e){
    e.preventDefault();

    submitting();

    jmbgErrors.innerHTML = '';
    emailErrors.innerHTML = '';
    confirmedErrors.innerHTML = '';

    var jmbgEl = document.querySelector('[name=jmbg]');
    var emailEl = document.querySelector('[name=email]');

    var jmbg = jmbgEl.value;
    var email = emailEl.value;    
    var confirmed = confirmBox.checked;

    var error = showErrorsForForm(jmbg, email, confirmed);

    if(error) return false;

    var data = JSON.stringify({email: email, jmbg: jmbg, confirmed: confirmed});

    var xhr = new XMLHttpRequest();
    xhr.open("POST", CHECK_URL, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function () {

        if(xhr.readyState !== 4 || xhr.status !== 200) {
            doneSubmitting();
            return;
        }

        var response = JSON.parse(xhr.responseText);

        if(!showErrorsForResponse(response)) {

            var xhrApply = new XMLHttpRequest();
            xhrApply.open("POST", APPLY_URL, true);
            xhrApply.setRequestHeader("Content-type", "application/json");

            xhrApply.onreadystatechange = function () {

                if(xhrApply.readyState !== 4 || xhrApply.status !== 200) {
                    doneSubmitting();
                    return;
                }

                var responseApply = JSON.parse(xhrApply.responseText);
                if(!showErrorsForResponse(responseApply) && responseApply.created) {
                    document.getElementById('applications-form-container').style.display = 'none';
                    document.getElementById('success-message-container').style.display = 'block';
                    window.location.hash = '#application-success';
                }

            };

            xhrApply.send(data);  

        } else {
            doneSubmitting();
        }
    };
    xhr.send(data); 
};

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validateJmbg(jmbg) {
    var re = /^\d{13}$/;
    return re.test(jmbg);
}
function showErrorsForResponse(response) {
    var responseError = false;
    if(!response.valid) {
        jmbgErrors.innerHTML = 'JMBG nije validan, ne postoji u bazi, ili je već prijavljen.';
        responseError = true;
    }
    if(response.exists) {
        emailErrors.innerHTML = 'Email postoji u bazi.';
        responseError = true;
    }    
    if(response.confirmed) {
        confirmedErrors.innerHTML = 'Potrebno je prihvatiti uvjete korištenja.';
        responseError = true;
    }
    return responseError;
}
function showErrorsForForm(jmbg, email, confirmed) {
    var error = false;
    if(!validateJmbg(jmbg)) {
        jmbgErrors.innerHTML = 'JMBG mora imati 13 brojeva.';
        error = true;
    }
    if(!validateEmail(email)) {
        emailErrors.innerHTML = 'Neispravan email.';
        error = true;
    }
    if(!confirmed) {        
        confirmedErrors.innerHTML = 'Potrebno je prihvatiti uvjete korištenja.';
        error = true;
    } 

    return error;
}