<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(JmbgSeeder::class);
        $this->call(ApplicationSeeder::class);
        $this->call(MarketingSeeder::class);
    }
}
