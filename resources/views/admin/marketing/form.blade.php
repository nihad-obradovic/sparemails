@extends('admin.layouts.dashboard')

@section('page_heading', 'Prijava Test')

@section('section')
    <div class="col-sm-6" id="marketing-form-container">
        <form name="marketing-form" id="marketing-form">
            @component('admin.widgets.panel')
                @slot('panelBody')
                    <div class="form-group">
                        <label>Ime i prezime: </label>
                        <input type="text" name="name" class="form-control">
                        <p id="name-errors" class="text-danger"></p>
                    </div>
                    <div class="form-group">
                        <label>E-Mail: </label>
                        <input type="text" name="email" class="form-control">
                        <p id="email-errors" class="text-danger"></p>
                    </div>
                    <div class="form-group">
                        <label>Telefon: </label>
                        <input type="text" name="phone" class="form-control">
                        <p id="phone-errors" class="text-danger"></p>
                    </div>
                    <div class="form-group">
                        <label>Starosna dob: </label>

                        <div class="radio">
                            <label>
                                <input type="radio" name="age" id="age1" value="1"> Manje od 25
                            </label>
                            &emsp;
                            <label>
                                <input type="radio" name="age" id="age2" value="2"> 25 - 40
                            </label>
                            &emsp;
                            <label>
                                <input type="radio" name="age" id="age3" value="3"> 40 - 60
                            </label>
                            &emsp;
                            <label>
                                <input type="radio" name="age" id="age4" value="4"> Preko 60
                            </label>
                        </div>

                        <p id="age-errors" class="text-danger"></p>
                    </div>
                    <div class="form-group">
                        <label>Da li ste klijent banke?</label>

                        <div class="radio">
                            <label>
                                <input type="radio" name="client" id="age1" value="1"> Da
                            </label>
                            &emsp;
                            <label>
                                <input type="radio" name="client" id="age2" value="0"> Ne
                            </label>
                        </div>

                        <p id="client-errors" class="text-danger"></p>
                    </div>
                    <div class="form-group">
                        <label>
                           <input type="checkbox" name="disclaimer" id="disclaimer"> Unosom gore navedenih podataka, potvrđujem da sam saglasan da Sparkasse Bank d.d. BiH čuva, obrađuje i koristi moje lične podatke iz ovog ispunjenog obrasca: ime i prezime/broj telefona/e-mail, a u svrhu učešća u nagradnoj igri koju organizira i provodi Sparkasse Bank dd BiH, kao i promotivnih i prodajnih aktivnosti, direktnog marketinga, analize i istraživanja. Ova saglasnost ne može se koristiti u druge svrhe od navedenih, a dajem je na neograničen vremenski period, do vlastitog opoziva iste.
                        </label>
                        <p id="disclaimer-errors" class="text-danger"></p>
                    </div>
                @endslot
                @slot('panelFooter')
                    <input type="submit" id="form-submit" class="btn btn-primary btn-sm" value="Dodaj" style="display:none">
                @endslot
            @endcomponent
        </form>
    </div>
    <div class="col-sm-6" id="success-message-container" style="display: none">
        <div class="alert alert-success" role="alert">
            Hvala Vam za učešće u nagradnoj igri.
        </div>
    </div>

    <script src="{{ asset("js/form-marketing.js") }}"></script>
@endsection
