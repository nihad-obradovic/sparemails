<?php namespace App\Importers;

use App\Jmbg;
use Maatwebsite\Excel\Facades\Excel;

class JmbgImporter
{
    protected $file;
    public $imported = 0;
    public $errors;

    public function importFile($file)
    {
        $this->file = $file;

        if(!in_array($this->file->getClientOriginalExtension(), ['csv', 'xlsx', 'xls']))
        {
            $this->errors[] = 'Fajl <strong>'.$this->file->getClientOriginalName().'</strong> nije ispravan file (CSV, XLSX).';
            return;
        }

        $dataToImport = $this->getData($this->file->getPathname());

        if(count($dataToImport) == 0)
        {
            $this->errors[] = 'Fajl <strong>'.$this->file->getClientOriginalName().'</strong> nema podataka za unos.';
            return;
        }

        $this->file->store('imported_files');

        $this->importData($dataToImport);
    }

    private function getData($filename = '')
    {
        $data = [];
        Excel::load($filename, function($reader) use (&$data) {

            // Getting all results
            $data = $reader->toArray();

        });

        return $data;
    }

    private function importData($dataToImport)
    {

        if(!isset($dataToImport[0]['jmbg']) )
        {
            $this->errors[] = 'Fajl <strong>'.$this->file->getClientOriginalName().'</strong> nema JMBG kolonu.';
            return;
        }

        $jmbgColumnKey = isset($dataToImport[0]['jmbg']) ? 'jmbg' : null;

        foreach ($dataToImport as $item)
        {
            $jmbgValue = $item[$jmbgColumnKey];

            if(strlen($jmbgValue) !== 13)
            {
                if(strlen($jmbgValue) == 12)
                {
                    $jmbgValue = '0' . $jmbgValue;
                } else
                {
                    $this->errors[] = 'Fajl <strong>'.$this->file->getClientOriginalName().'</strong> sadrži neispravan JMBG <strong>'.$jmbgValue.'</strong>.';
                    continue;
                }
            }

            $exists = Jmbg::where('jmbg', $jmbgValue)->count();

            if($exists == 0)
            {
                $created = Jmbg::create([
                    'jmbg'=>$jmbgValue,
                    'import_data' => $item
                ]);
                if($created)
                {
                    $this->imported++;
                }
            } else {
                $this->errors[] = 'JMBG <strong>'.$jmbgValue.'</strong> već postoji.';
            }
        }
    }
}