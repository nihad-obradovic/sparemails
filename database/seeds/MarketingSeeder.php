<?php

use App\User;
use Illuminate\Database\Seeder;

class MarketingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'name' => 'Marketing',
            'email' => 'marketing@admin.com',
            'password' => bcrypt('Marketing2017?'),
            'marketing' => true
        ]);
        $user->save();
    }
}
