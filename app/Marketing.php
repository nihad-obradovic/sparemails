<?php

namespace App;

use App\Traits\SortableTrait;
use Illuminate\Database\Eloquent\Model;

class Marketing extends Model
{
    use SortableTrait;

    protected $table = 'marketing';

    protected $fillable = [
        'name', 'email', 'age', 'phone', 'client', 'disclaimer', 'ip_address'
    ];

    public static $_AGES = [
        1 => 'Manje od 25',
        2 =>  '25 - 40',
        3 =>  '40 - 60',
        4 =>  'Preko 60'
    ];

    public static function filterFromRequest($request)
    {
        $data = static::sortable();

        if(isset($request))
        {
            if(isset($request['date_from']))
            {
                $data = $data->whereDate('created_at', '>=', $request['date_from']);
            }
            if(isset($request['date_to']))
            {
                $data = $data->whereDate('created_at', '<=', $request['date_to']);
            }
        }

        return $data;
    }

    public static function total()
    {
        return static::all()->count();
    }

    public static function createFromRequest($request)
    {
        $created = self::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'age' => $request->input('age'),
            'client' => $request->input('client'),
            'disclaimer' => $request->input('disclaimer'),
            'ip_address' => $request->has('ip') ? $request->input('ip') : $request->ip()
        ]);

        return !!$created;
    }

    public function getAgeTitleAttribute()
    {
        return static::$_AGES[$this->age];
    }

}
