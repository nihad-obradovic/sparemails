<form name="jmbg-import" method="post" action="{{route('admin.jmbgs.import')}}" enctype="multipart/form-data">
    @component('admin.widgets.panel')
        @slot('panelTitle')
            Import
        @endslot
        @slot('panelBody')
            {{csrf_field()}}
            <div class="form-group">
                <label>Odaberite fajl: </label>
                <input type="file" name="import_files[]" class="form-control" multiple="">
            </div>
        @endslot
        @slot('panelFooter')
            <input type="submit" class="btn btn-primary btn-sm" value="Importuj">
        @endslot
    @endcomponent
</form>