<?php

namespace App\Http\ViewComposers\Admin;

use App\Application;
use App\Jmbg;
use Illuminate\View\View;

class DashboardComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('totalJmbgs', Jmbg::totalJmbgs());
        $view->with('totalConfirmedJmbgs', Jmbg::totalConfirmedJmbgs());
        $view->with('totalApplications', Application::totalApplications());
    }
}