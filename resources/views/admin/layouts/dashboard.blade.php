@extends('admin.layouts.app')

@section('body')
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url ('/admin') }}">{{ config('app.name', 'SparEmails') }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> {{auth()->user()->name}}&nbsp;<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('admin.logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out fa-fw"></i>  Logout
                            </a>

                            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        @if(! auth()->user()->marketing)
                            <li {{ (Request::is('/') ? 'class="active"' : '') }}>
                                <a href="{{ url ('admin/dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Info</a>
                            </li>
                            <li {{ (Request::is('admin.jmbgs') ? 'class="active"' : '') }}>
                                <a href="{{ route ('admin.jmbgs') }}"><i class="fa fa-list-ol fa-fw"></i> Matični brojevi</a>
                                <!-- /.nav-second-level -->
                            </li>
                            <li {{ (Request::is('admin.applications.list') ? 'class="active"' : '') }}>
                                <a href="{{ route ('admin.applications.list') }}"><i class="fa fa-envelope fa-fw"></i> Prijave emailova</a>
                                <!-- /.nav-second-level -->
                            </li>
                            <li {{ (Request::is('admin.applications.form') ? 'class="active"' : '') }}>
                                <a href="{{ route ('admin.applications.form') }}"><i class="fa fa-envelope fa-fw"></i> Prijava test</a>
                                <!-- /.nav-second-level -->
                            </li>
                        @else
                            <li {{ (Request::is('admin.marketing.list') ? 'class="active"' : '') }}>
                                <a href="{{ route ('admin.marketing.list') }}"><i class="fa fa-list-ol fa-fw"></i> Prijave</a>
                                <!-- /.nav-second-level -->
                            </li>
                            <li {{ (Request::is('admin.marketing.form') ? 'class="active"' : '') }}>
                                <a href="{{ route ('admin.marketing.form') }}"><i class="fa fa-keyboard-o fa-fw"></i> Forma za prijavu</a>
                                <!-- /.nav-second-level -->
                            </li>
                        @endif
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('page_heading')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                @yield('section')
            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
@endsection
