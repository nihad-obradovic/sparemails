<?php

namespace App\Http\Controllers\Admin;

use App\Marketing;
use Excel;
use App\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarketingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $input = $request->input();
        $data = Marketing::filterFromRequest($input)->paginate(15);

        $total = Marketing::total();

        return view('admin.marketing.list', compact('data', 'input', 'total'));
    }

    public function export(Request $request)
    {
	    $input = $request->input();
        $marketing = Marketing::filterFromRequest($input)->get()->toArray();

        if(count($marketing) == 0)
        {
            return redirect()->back();
        }

        $data = [];
        foreach ($marketing as $key => $value) {
            $data[] = [
                'Ime i Prezime' => $value['name'],
                'E-Mail' => $value['email'],
                'Telefon' => $value['phone'],
                'Starosna dob' => Marketing::$_AGES[$value['age']],
                'Klijent banke' => $value['client'] ? 'DA' : 'NE',
                'Disclaimer' => $value['disclaimer'] ? 'DA' : 'NE',
                'IP' => $value['ip_address']
            ];
        }

        $excel = Excel::create('export_'.date('Ymd_Hi'), function($excel) use ($data)
        {
            $excel->sheet('Sheet 1', function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        });

        // work on the export
        return $excel->download('xls');
    }

    public function form()
    {
        return view('admin.marketing.form');
    }
}
