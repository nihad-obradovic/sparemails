<?php

namespace App\Http\ViewComposers\Admin;

use App\Application;
use App\Jmbg;
use App\Marketing;
use Illuminate\View\View;

class MarketingComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('totalMarketing', Marketing::total());
    }
}