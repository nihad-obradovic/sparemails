<?php

use Illuminate\Database\Seeder;

class JmbgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Jmbg::class, 50)->create();
    }
}
