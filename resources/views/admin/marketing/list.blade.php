@extends('admin.layouts.dashboard')

@section('page_heading','Prijave')

@section('section')
    <div class="col-sm-12 col-md-10 col-lg-8">
        @component('admin.widgets.panel')
            @slot('panelBody')
                @include('admin.marketing._filter', [
                    'date_from' => isset($input['date_from']) ? $input['date_from'] : '',
                    'date_to' => isset($input['date_to']) ? $input['date_to'] : ''
                ])
            @endslot
            @slot('panelTable')
                @include('admin.marketing._table', ['data'=>isset($data) ? $data : []])
                <div class="text-center">
                    {{$data->links()}}
                </div>
            @endslot
            @slot('panelFooter')
                Ukupno prijava: <strong>{{$total}}</strong>
            @endslot
        @endcomponent
    </div><!-- /.col-sm-6 -->
    <div class="col-sm-12 col-md-2 col-lg-4">
        @include('admin.marketing._export', [
            'date_from' => isset($input['date_from']) ? $input['date_from'] : '',
            'date_to' => isset($input['date_to']) ? $input['date_to'] : ''
        ])
    </div><!-- /.col-sm-4 -->
@endsection
