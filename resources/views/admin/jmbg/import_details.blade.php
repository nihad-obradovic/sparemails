@extends('admin.layouts.dashboard')

@section('page_heading','Detalji importa matičnih brojeva')

@section('section')


    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6">
                <p style="margin-bottom: 22px;"><a href="{{route('admin.jmbgs')}}" class="btn btn-info">Nazad</a></p>
                @component('admin.widgets.panel')
                    @slot('panelTitle')
                        Uspješno importovano: <strong>{{$imported}}</strong>
                    @endslot
                @endcomponent
                @if(count($errors) > 0)
                    @component('admin.widgets.panel')
                        @slot('panelTitle')
                            Greške prilikom importa
                        @endslot
                        @slot('panelList')
                            @if(count($errors) > 0)
                                @foreach($errors as $error)
                                    <li class="list-group-item">
                                        {!! $error !!}
                                    </li>
                                @endforeach
                            @endif
                        @endslot
                    @endcomponent
                @endif
            </div>
        </div>
    </div>

@endsection