<?php

namespace App\Traits;

use Illuminate\Support\Facades\Input;

trait SortableTrait
{
    public function scopeSortable($query)
    {
        if(Input::has('s') && Input::has('o'))
        {
            return $query->orderBy(Input::get('s'), Input::get('o'));
        } else
        {
            return $query;
        }
    }

    public static function linkZoSortingAction($col, $title = null)
    {
        if (is_null($title)) {
            $title = str_replace('_', ' ', $col);
            $title = ucfirst($title);
        }

        $parameters = array_merge(Input::get(), [
            's' => $col,
            'o' => (Input::get('o') === 'asc' ? 'desc' : 'asc')
        ]);

        return '<a href="'.route(\Route::currentRouteName(), $parameters).'">'.$title.'</a>';
    }
}