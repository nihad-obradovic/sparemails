<form name="applications-export" method="post" action="{{route('admin.applications.export')}}">
    @component('admin.widgets.panel')
        @slot('panelTitle')
            Export
        @endslot
        @slot('panelBody')
            {{csrf_field()}}
            <div class="form-group">
                <label>Datum od: </label>
                <input type="text" name="date_from" class="form-control datepicker" value="{{$date_from}}">
            </div>
            <div class="form-group">
                <label>Datum do: </label>
                <input type="text" name="date_to" class="form-control datepicker" value="{{$date_to}}">
            </div>
        @endslot
        @slot('panelFooter')
            <input type="submit" class="btn btn-primary btn-sm" value="Exportuj">
        @endslot
    @endcomponent
</form>
