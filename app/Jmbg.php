<?php

namespace App;

use App\Traits\SortableTrait;
use Illuminate\Database\Eloquent\Model;

class Jmbg extends Model
{
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jmbg', 'email', 'import_data'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'import_data' => 'array',
    ];

    /**
     * Scope a query to only include confirmed jmbgs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApplied($query)
    {
        return $query->where('applied', 1);
    }

    /**
     * STATIC FUNCTION
     */

    public static function totalJmbgs()
    {
        return static::all()->count();
    }

    public static function totalConfirmedJmbgs()
    {
        return static::applied()->count();
    }
}
