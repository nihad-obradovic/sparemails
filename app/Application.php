<?php

namespace App;

use BrowserDetect;
use App\Traits\SortableTrait;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Agent\Agent;

class Application extends Model
{
    use SortableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jmbg', 'email', 'confirmed', 'browser_info', 'ip_address'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'browser_info' => 'array',
    ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    public static function filterFromRequest($request)
    {
        $data = static::sortable();

        if(isset($request))
        {
            if(isset($request['date_from']))
            {
                $data = $data->whereDate('created_at', '>=', $request['date_from']);
            }
            if(isset($request['date_to']))
            {
                $data = $data->whereDate('created_at', '<=', $request['date_to']);
            }
        }

        return $data;
    }

    public static function createFromRequest($request)
    {
        $agent = new Agent();
        $browserInfo = [
            'mobile' => $agent->isMobile(),
            'tablet' => $agent->isTablet(),
            'desktop' => $agent->isDesktop(),
            'device' => $agent->device(),
            'platform' => $agent->platform(),
            'platform_version' => $agent->version($agent->platform()),
            'browser' => $agent->browser(),
            'browser_version' => $agent->version($agent->browser()),
            'robot' => $agent->isRobot(),
            'robot_name' => $agent->robot()
        ];

        $created = self::create([
            'jmbg' => $request->input('jmbg'),
            'email' => $request->input('email'),
            'confirmed' => ($request->input('confirmed') == 'true') ? 1 : 0,
            'browser_info' => $browserInfo,
            'ip_address' => $request->ip()
        ]);

        if(!!$created)
        {
            $jmbg = Jmbg::where('jmbg', $request->input('jmbg'))
                ->where('applied', 0)
                ->first();
            $jmbg->applied = true;
            $jmbg->save();
        }

        return !!$created;
    }

    public static function totalApplications()
    {
        return self::all()->count();
    }
}
